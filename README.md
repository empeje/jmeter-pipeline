# Load Testing with BlazeMeter and OctoPerf

This repository provides a framework for performing load testing using BlazeMeter and OctoPerf, integrated with GitLab CI for automated test execution. The setup uses Taurus (bzt) as the testing tool, allowing for flexible and powerful load testing scenarios.

## High-Level Overview

The core functionality of this repository is to automate load testing processes with the following key components:

- **BlazeMeter and OctoPerf Integration**: Two popular load testing platforms integrated into the CI/CD pipeline.
- **Taurus (bzt)**: A tool used to run the load tests.
- **GitLab CI**: Automates the execution of load tests, collects and compares test results.

## Repository Structure

```
├── .git
├── .gitignore
├── .gitlab-ci.yml
├── README.md
├── configs
│   ├── blazedemo_executor.yaml
│   └── octoperf_executor.yaml
├── fixtures
├── jmeter.log
├── output
├── requirements.txt
├── scenarios
│   ├── ThroughputController.jmx
│   ├── blazedemo.yaml
│   └── octoperf.yaml
├── venv
```

- **configs**: Configuration files for different test executors.
- **scenarios**: Load test scenario files.
- **output**: Directory for test outputs.
- **requirements.txt**: Python dependencies.
- **fixtures**: Additional files required for the test.

## Development Setup

### Prerequisites

- Python 3.6+
- Virtualenv (recommended for creating an isolated Python environment)

### Installing Dependencies

1. **Clone the repository**:

   ```bash
   git clone <repository_url>
   cd <repository_directory>
   ```

2. **Create and activate a virtual environment**:

   ```bash
   python -m venv venv
   source venv/bin/activate   # On Windows: venv\Scripts\activate
   ```

3. **Install the required Python packages**:

   ```bash
   pip install -r requirements.txt
   ```

### Running Tests Locally

To run load tests locally using Taurus (bzt):

1. **BlazeMeter Scenario**:

   ```bash
   bzt ./configs/blazedemo_executor.yaml ./scenarios/blazedemo.yaml
   ```

2. **OctoPerf Scenario**:

   ```bash
   bzt ./configs/octoperf_executor.yaml ./scenarios/octoperf.yaml
   ```

## GitLab CI Configuration

The `.gitlab-ci.yml` file is set up to automate load testing. It defines stages and jobs to execute BlazeMeter and OctoPerf scenarios, manage test artifacts, and compare results.

Please note that artifact has expiry, so sometime the job might recognize that the old kpi is not found.

### Key Components

- **Default Load Test Configuration**: Specifies the Docker image, scripts to run before and after tests, and artifact management.
- **Stages**: Defines the load test stage.
- **Jobs**: Defines individual jobs for BlazeMeter and OctoPerf scenarios.

## How to Use CI/CD Pipeline

1. **Push Changes**: Commit and push your changes to the repository.
2. **CI Pipeline**: The GitLab CI pipeline will automatically run the configured load tests.
3. **Review Artifacts**: After the pipeline execution, review the generated artifacts (e.g., `output/stats.xml`) for test results.

## Contributing

Contributions are welcome! Please open an issue to discuss your ideas or submit a pull request.

## License

This project is licensed under the MIT License. See the `LICENSE` file for more details.

---

This README provides a high-level idea of the repository and instructions on how to set up the development environment, install dependencies, and run load tests locally. For more detailed CI/CD configurations, refer to the `.gitlab-ci.yml` file.